import 'package:flutter/material.dart';

class navigationRailBarWidget extends StatefulWidget {
  const navigationRailBarWidget({Key? key}) : super(key: key);

  @override
  State<navigationRailBarWidget> createState() =>
      _navigationRailBarWidgetState();
}

class _navigationRailBarWidgetState extends State<navigationRailBarWidget> {
  bool _extended = false;

  void _extendMenu() {
    setState(() {
      _extended = !_extended;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_extended) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              children: [
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.verified_user),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.account_balance),
                ),
              ],
            ),
          ),
          Center(
            child: OutlinedButton(
              onPressed: _extendMenu,
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.grey[50]),
                foregroundColor: MaterialStateProperty.all(Colors.black),
                shadowColor: MaterialStateProperty.all(Colors.grey[50]),
                side: MaterialStateProperty.all(
                  const BorderSide(color: Colors.white),
                ),
              ),
              child: const Text(
                ">",
                style: TextStyle(fontSize: 24.0),
              ),
            ),
          ),
        ],
      );
    } else {
      return SizedBox(
        width: 200,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: OutlinedButton.icon(
                      onPressed: () {},
                      icon: const Icon(Icons.verified_user),
                      label: const Text(
                        "Caca pouet",
                        style: TextStyle(fontSize: 18.0),
                      ),
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.grey[50]),
                        foregroundColor:
                            MaterialStateProperty.all(Colors.black),
                        shadowColor: MaterialStateProperty.all(Colors.white),
                        side: MaterialStateProperty.all(
                            const BorderSide(color: Colors.white)),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: OutlinedButton.icon(
                      onPressed: () {},
                      icon: const Icon(Icons.account_balance),
                      label: const Text(
                        "Caca boudin",
                        style: TextStyle(fontSize: 18.0),
                      ),
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.grey[50]),
                        foregroundColor:
                            MaterialStateProperty.all(Colors.black),
                        shadowColor: MaterialStateProperty.all(Colors.white),
                        side: MaterialStateProperty.all(
                            const BorderSide(color: Colors.white)),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Center(
              child: OutlinedButton(
                onPressed: _extendMenu,
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.grey[50]),
                  foregroundColor: MaterialStateProperty.all(Colors.black),
                  shadowColor: MaterialStateProperty.all(Colors.grey[50]),
                  side: MaterialStateProperty.all(
                      const BorderSide(color: Colors.white)),
                ),
                child: const Text(
                  "<",
                  style: TextStyle(fontSize: 24.0),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }
}
